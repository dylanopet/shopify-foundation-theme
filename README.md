Shopify Foundation Theme
========================

Hey hey hey ;)

This theme is built almost entirely from the Zurb Foundation framework, and Shopify liquid markup, with a few little extra touches added for good measure.

The template is intended as a rapid Shopify theme development framework for developers.

Check out the demo at http://foundation-theme.myshopify.com
